import 'dart:collection';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:hwg_glass/consts.dart';
import 'package:hwg_glass/data/task.dart';
import 'package:hwg_glass/utils.dart';
import 'package:hwg_glass/video_listing.dart';
import 'package:hwg_glass/video_player.dart';
import 'package:hwg_glass/view/pdf_view.dart';
import 'package:hwg_glass/view/task_view.dart';
import 'package:localstorage/localstorage.dart';

import 'qr_view.dart';

const isrgRootX1 = '''-----BEGIN CERTIFICATE-----
MIIFazCCA1OgAwIBAgIRAIIQz7DSQONZRGPgu2OCiwAwDQYJKoZIhvcNAQELBQAw
TzELMAkGA1UEBhMCVVMxKTAnBgNVBAoTIEludGVybmV0IFNlY3VyaXR5IFJlc2Vh
cmNoIEdyb3VwMRUwEwYDVQQDEwxJU1JHIFJvb3QgWDEwHhcNMTUwNjA0MTEwNDM4
WhcNMzUwNjA0MTEwNDM4WjBPMQswCQYDVQQGEwJVUzEpMCcGA1UEChMgSW50ZXJu
ZXQgU2VjdXJpdHkgUmVzZWFyY2ggR3JvdXAxFTATBgNVBAMTDElTUkcgUm9vdCBY
MTCCAiIwDQYJKoZIhvcNAQEBBQADggIPADCCAgoCggIBAK3oJHP0FDfzm54rVygc
h77ct984kIxuPOZXoHj3dcKi/vVqbvYATyjb3miGbESTtrFj/RQSa78f0uoxmyF+
0TM8ukj13Xnfs7j/EvEhmkvBioZxaUpmZmyPfjxwv60pIgbz5MDmgK7iS4+3mX6U
A5/TR5d8mUgjU+g4rk8Kb4Mu0UlXjIB0ttov0DiNewNwIRt18jA8+o+u3dpjq+sW
T8KOEUt+zwvo/7V3LvSye0rgTBIlDHCNAymg4VMk7BPZ7hm/ELNKjD+Jo2FR3qyH
B5T0Y3HsLuJvW5iB4YlcNHlsdu87kGJ55tukmi8mxdAQ4Q7e2RCOFvu396j3x+UC
B5iPNgiV5+I3lg02dZ77DnKxHZu8A/lJBdiB3QW0KtZB6awBdpUKD9jf1b0SHzUv
KBds0pjBqAlkd25HN7rOrFleaJ1/ctaJxQZBKT5ZPt0m9STJEadao0xAH0ahmbWn
OlFuhjuefXKnEgV4We0+UXgVCwOPjdAvBbI+e0ocS3MFEvzG6uBQE3xDk3SzynTn
jh8BCNAw1FtxNrQHusEwMFxIt4I7mKZ9YIqioymCzLq9gwQbooMDQaHWBfEbwrbw
qHyGO0aoSCqI3Haadr8faqU9GY/rOPNk3sgrDQoo//fb4hVC1CLQJ13hef4Y53CI
rU7m2Ys6xt0nUW7/vGT1M0NPAgMBAAGjQjBAMA4GA1UdDwEB/wQEAwIBBjAPBgNV
HRMBAf8EBTADAQH/MB0GA1UdDgQWBBR5tFnme7bl5AFzgAiIyBpY9umbbjANBgkq
hkiG9w0BAQsFAAOCAgEAVR9YqbyyqFDQDLHYGmkgJykIrGF1XIpu+ILlaS/V9lZL
ubhzEFnTIZd+50xx+7LSYK05qAvqFyFWhfFQDlnrzuBZ6brJFe+GnY+EgPbk6ZGQ
3BebYhtF8GaV0nxvwuo77x/Py9auJ/GpsMiu/X1+mvoiBOv/2X/qkSsisRcOj/KK
NFtY2PwByVS5uCbMiogziUwthDyC3+6WVwW6LLv3xLfHTjuCvjHIInNzktHCgKQ5
ORAzI4JMPJ+GslWYHb4phowim57iaztXOoJwTdwJx4nLCgdNbOhdjsnvzqvHu7Ur
TkXWStAmzOVyyghqpZXjFaH3pO3JLF+l+/+sKAIuvtd7u+Nxe5AW0wdeRlN8NwdC
jNPElpzVmbUq4JUagEiuTDkHzsxHpFKVK7q4+63SM1N95R1NbdWhscdCb+ZAJzVc
oyi3B43njTOQ5yOf+1CceWxG1bQVs5ZufpsMljq4Ui0/1lvh+wjChP4kqKOJ2qxq
4RgqsahDYVvTH9w7jXbyLeiNdd8XM2w9U/t7y0Ff/9yi0GE44Za4rF2LN9d11TPA
mRGunUHBcnWEvgJBQl9nJEiU0Zsnvgc/ubhPgXRR4Xq37Z0j4r7g1SgEEzwxA57d
emyPxgcYxn/eR44/KJ4EBs+lVDR3veyJm+kXQ99b21/+jh5Xos1AnX5iItreGCc=
-----END CERTIFICATE-----
''';

final DEBUG_SETUP = false;

String qrScanResult = "";
final bool VOICE_RECOGNITION_ENABLED = false;

void main() {

  SecurityContext.defaultContext.setTrustedCertificatesBytes(Uint8List.fromList(isrgRootX1.codeUnits));
  runApp(HWGGlass());


}

class HWGGlass extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HWGGlassState();
  }
}

void nothing(_) {
  //TODO REMOVE THIS AS SOON EVERYTHING IS IMPLEMENTED
}

void _showVideos(BuildContext context) async {
  final result = await Navigator.of(context).push(MaterialPageRoute(builder: (context) => VideoListing()));
  if ((result ?? "").length > 0) {
    if ((result as String).toLowerCase().endsWith(".pdf")) {
      Navigator.of(context).push(MaterialPageRoute(builder: (context) => PDFViewer(result)));
    } else {
      Navigator.of(context).push(MaterialPageRoute(builder: (context) => VideoPlayer(result, 0, isLocalFile: true)));
    }
  }
}

void _exitApp(BuildContext context) {
  exit(0);
}

class _HWGGlassState extends State<HWGGlass> {
  int currentIndex = 0;

  final CarouselController _controller = CarouselController();
  CarouselSlider carouselSlider;
  List<CarouselElement> carouselElements = [];

  List<Task> listOfTasks = [];

  FlutterSecureStorage storage;
  LocalStorage localStorage = LocalStorage("tasks.json");

  //VoiceBackgroundSTT vbs;

  void getRegisteredUserFromStorage() {
    storage.read(key: "name").then((value) {
      if (value == null) {
        return;
      }
      setState(() {
        Utils.userName = value;
        fetchLocalStorageTasks();
        _fetchTasks(context, withThrobber: true);
      });
    });
  }

  void fetchLocalStorageTasks() async {
    this.localStorage.ready.whenComplete(() {
      this.listOfTasks = [];
      String str = localStorage.getItem("tasks");
      debugPrint(str);
      if (str == null) {
        return;
      }
      dynamic json = jsonDecode(str);
      for (dynamic entry in json['tasks']) {
        this.listOfTasks.add(Task.fromJson(Task(), entry));
      }
    });
  }

  @override
  initState()  {
    super.initState();

    storage = FlutterSecureStorage();

    getRegisteredUserFromStorage();
    this.carouselElements = [
      CarouselElement("QR Code", Icons.qr_code, COLOR_YELLOW, scan),
      CarouselElement("BauDocs", Icons.ondemand_video, COLOR_BLUE, _showVideos),
      // CarouselElement("Spracheingabe", Icons.mic, COLOR_GREEN, nothing),
      CarouselElement("Remote Assist", Icons.help_outline, COLOR_GREEN, nothing),
      // CarouselElement("Remote Assist", ImageIcon(AssetImage
      //   ("assets/remote_assist_icon_white.png"), color: Colors.white),
      //   COLOR_GREEN,nothing),
      // CarouselElement("Tasks", Icons.pending_actions, COLOR_ORANGE, _showTasks),
      CarouselElement("Aufträge", Icons.assignment, COLOR_ORANGE, _showTasks),
      CarouselElement("Aufträge abrufen", Icons.assignment_returned, COLOR_PURPLE, _fetchTasks),
      CarouselElement("Beenden", Icons.power_settings_new, COLOR_RED, _exitApp),
    ];
    // if (VOICE_RECOGNITION_ENABLED) {
    //   vbs = VoiceBackgroundSTT();
    //   vbs.startListening();
    //   vbs.listenToCommands({"play": play, "pause": pause, "vor": forward, "zurück": backward});
    // }

    //first time download of all tasks for today
    _fetchTasks(context);
  }

  Widget returnText() {
    return Container(
      child: SizedBox(
        width: 640,
        height: 360,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "Bitte QR Code einscannen um sich zu registrieren",
              style: TextStyle(
                color: Colors.white,
                decoration: TextDecoration.none,
                fontSize: 20,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Text(
                "(Tap um Scanner zu aktivieren!!!)",
                style: TextStyle(
                  color: Colors.red,
                  decoration: TextDecoration.none,
                  fontSize: 16,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void scan(BuildContext context) async {
    String result = await Navigator.of(context).push(MaterialPageRoute(builder: (context) => QRScanView()));

    if (result == null || result.isEmpty) {
      return;
    }
    //Progress Indicator
    showThrobber(context);

    LinkedHashMap jsonObject = json.decode(result);
    if (jsonObject.containsKey("type")) {
      String type = jsonObject["type"];
      if (type == "video") {
        //{"type":"video","url":"https://www.youtube.com/watch?v=uyzoSqjQgVI","time":0}
        String url = jsonObject["url"];
        int time = jsonObject["time"] ?? 0;

        if (url.contains("www.youtube")) {
          //download movie with 360p from youtube
          String filePath = await Utils.downloadMovie(url);

          Navigator.of(context)
              .push(MaterialPageRoute(builder: (context) => VideoPlayer(filePath, time, isLocalFile: true)));
          removeThrobber();
        } else {
          Navigator.of(context).push(MaterialPageRoute(builder: (context) => VideoPlayer(url, time)));
          removeThrobber();
        }
      } else if (type == "setup") {
        String name = jsonObject['name'];
        String email = jsonObject['email'];
        String password = jsonObject['password'];
        String imap = jsonObject['imap'];
        String imapPort = jsonObject['imapPort'];
        String smtp = jsonObject['smtp'];
        String smtpPort = jsonObject['smtpPort'];

        storage.write(key: "name", value: name);
        storage.write(key: "email", value: email);
        storage.write(key: "password", value: password);
        storage.write(key: "imap", value: imap);
        storage.write(key: "imapPort", value: imapPort);
        storage.write(key: "smtp", value: smtp);
        storage.write(key: "smtpPort", value: smtpPort);
        setState(() {
          Utils.userName = name;
        });
        _fetchTasks(context);
      }
    }
    removeThrobber();
  }

  @override
  Widget build(BuildContext context) {
    carouselSlider = CarouselSlider(
      key: Key("Carousel"),
      options: CarouselOptions(
        onPageChanged: pageChanged,
        height: 240.0,
        enlargeCenterPage: true,
        autoPlay: false,
      ),carouselController: _controller,
      items: carouselElements.map((i) {
        return Builder(
          builder: (BuildContext context) {
            return Container(
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  color: i.color,
                  borderRadius: BorderRadius.circular(20),
                  border: Border.all(color: Colors.white),
                ),
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      getIconWithShadow(
                        i.icon,
                        Colors.white,
                        150,
                      ),
                      Text(
                        '${i.title}',
                        style: TextStyle(
                          fontSize: 25.0,
                          color: Colors.white,
                          decoration: TextDecoration.none,
                        ),
                      ),
                    ],
                  ),
                ));
          },
        );
      }).toList(),
    );

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Builder(
        builder: (context) {
          return GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () {
              if (Utils.userName == null || Utils.userName.isEmpty) {
                //todo start qr code reader for installation
                this.scan(context);
              } else {
                onClick(context);
              }
            },
            child: (Utils.userName == null || Utils.userName.isEmpty) ? returnText() : Column(children: [carouselSlider,Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Flexible(
                    child: ElevatedButton(
                      onPressed: () => _controller.previousPage(),
                      child: Text('←'),
                    ),
                  ),
                  Flexible(
                    child: ElevatedButton(
                      onPressed: () => _controller.nextPage(),
                      child: Text('→'),
                    ),
                  )])])
          );
        },
      ),
    );
  }

  void onClick(BuildContext context) {
    carouselElements[currentIndex].action(context);
  }

  void pageChanged(int index, _) {
    currentIndex = index;
  }

  void _fetchTasks(BuildContext context, {bool withThrobber = true}) async {
    if (withThrobber) {
      showThrobber(context);
    }

    this.listOfTasks = await Task.fetchTasksFromInbox(DateTime.now());

    String jsonString = '{"tasks": [';
    for (int i = 0; i < listOfTasks.length; i++) {
      jsonString += listOfTasks[i].jsonString;
      if (i < listOfTasks.length - 1) {
        jsonString += ',';
      }
    }
    jsonString += "]}";

    //todo save the tasks
    localStorage.setItem("tasks", jsonString, (val) {
      return val;
    });
    if (withThrobber) {
      removeThrobber();
    }
  }

  void _showTasks(BuildContext context) async {
    await Navigator.of(context).push(MaterialPageRoute(builder: (context) => TaskView(listOfTasks)));
  }
}

class CarouselElement {
  String title;
  IconData icon;
  Color color;
  Function action;

  CarouselElement(this.title, this.icon, this.color, this.action);
}
