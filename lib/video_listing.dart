import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hwg_glass/consts.dart';
import 'package:hwg_glass/utils.dart';

class VideoListing extends StatefulWidget {
  List<String> listGtinFilter;

  VideoListing({this.listGtinFilter = const []});

  @override
  State<StatefulWidget> createState() {
    return _VideoListingState();
  }
}

class _VideoListingState extends State<VideoListing> {
  List<String> fileNames = [];
  int selectedIndex = 0;
  int newSelectedIndex = -1;

  double dragHorizontalStartPosition = 0;

  double dragVerticalStartPosition = 0;
  double dragVerticalEndPosition = 0;

  List<String> directories = [MOVIE_FOLDER];

  @override
  void initState() {
    super.initState();
    fetchDirectoryEntries();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          checkBauDocsExists(fileNames.length),
          ListView.builder(
            padding: EdgeInsets.only(left: 10, right: 10, top: 10),
            itemCount: fileNames.length,
            itemBuilder: (_, int index) {
              return GestureDetector(
                  child: ListTile(
                          title: Text(fileNames[index].split("/").last),
                          hoverColor: COLOR_RED,
                          tileColor: COLOR_BLUE,
                          textColor: Colors.white,
                          onTap: () {
                            selectedIndex = index;
                            if (fileNames[selectedIndex].endsWith("..")) {
                              directories.removeLast();
                              fetchDirectoryEntries();
                            } else if (Utils.isDirectory(
                                fileNames[selectedIndex])) {
                              directories.add(fileNames[selectedIndex]);
                              fetchDirectoryEntries();
                            } else {
                              Navigator.pop(context, fileNames[selectedIndex]);
                            }
                          }));
            },
          ),
        ],
      ),
    );
  }

  Widget checkBauDocsExists(int length) {
    if (length == 0)
      return Padding(
          child: Text("keine BauDocs vorhanden",
              textAlign: TextAlign.center,
              style:
                  const TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
          padding: EdgeInsets.all(20.0));
    else
      return Text("");
  }

  List<String> fetchDirectoryEntries() {
    selectedIndex = 0;
    var dir = Directory(directories.last);
    List<String> entries = [];
    if (directories.length > 1) {
      entries.add("..");
    }
    dir
        .list(followLinks: true, recursive: false)
        .listen((FileSystemEntity entity) {
      if (directories.length == 1 && this.widget.listGtinFilter.length > 0) {
        bool found = false;
        for (int i = 0; i < this.widget.listGtinFilter.length && !found; i++) {
          found = entity.path.indexOf(this.widget.listGtinFilter[i]) >= 0;
          if (found) {
            entries.add(entity.path);
          }
        }
      } else {
        entries.add(entity.path);
      }
      setState(() {
        fileNames = [];
        fileNames.addAll(entries);
      });
    });
  }
}
