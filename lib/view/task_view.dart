import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hwg_glass/consts.dart';
import 'package:hwg_glass/data/task.dart';
import 'package:hwg_glass/utils.dart';
import 'package:hwg_glass/view/task_detail_view.dart';
import 'package:intl/intl.dart';

class TaskView extends StatefulWidget {
  List<Task> listOfTasks = [];

  TaskView(this.listOfTasks);

  @override
  State<StatefulWidget> createState() {
    return _taskViewState();
  }
}

class _taskViewState extends State<TaskView> {
  bool currentSlotSet = false;
  final Color myGray = Color.fromARGB(255, 100, 100, 100);
  String userName = "";


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: COLOR_BLUE,
      appBar: AppBar(
        backgroundColor: COLOR_BLUE,
        centerTitle: true,
        title: Text("Aufträge - ${Utils.userName}"),
      ),
      body: Stack(children: [
        getTasks(),
      ]),
    );
  }

  Widget getTasks() {
    currentSlotSet = false;
    DateTime now = DateTime.now();
    return Scaffold(
        body: Stack(
        children: [
    ListView.builder(
      padding: EdgeInsets.only(left: 10, right: 10, top: 10),
      itemCount: widget.listOfTasks.length,
      itemBuilder: (_, int index) {
        return GestureDetector(
            child: ListTile(
                title: Text(myDTFormat(widget.listOfTasks[index].startTime, widget.listOfTasks[index].duration) +"\n"+ widget.listOfTasks[index].todo),
                subtitle: Text(widget.listOfTasks[index].client.name +", "+ widget.listOfTasks[index].client.address ),
                leading: getIcon(now, widget.listOfTasks[index]),
                tileColor: getColor(now, widget.listOfTasks[index], index),
                textColor: getTextColor(now, widget.listOfTasks[index], index),
                onTap: () {
                    Navigator.of(context)
                        .push(MaterialPageRoute(builder: (context) => TaskDetailView(this.widget.listOfTasks[index])));
                }
                ));
      },
    )]));
  }

  String myDTFormat(DateTime dt, int duration) {
    return DateFormat("HH:mm").format(dt) + " - " + DateFormat("HH:mm").format(dt.add(Duration(minutes: duration)));
  }

  Color getColor(DateTime now, Task t, int index) {

    if (t.startTime.compareTo(now) <= 0) {
      if (t.startTime.add(Duration(minutes: t.duration)).compareTo(now) >= 0) {
        currentSlotSet = true;
        return COLOR_GREEN;
      } else {
        return COLOR_PURPLE;
      }
    } else {
      if (!currentSlotSet) {
        currentSlotSet = true;
        return COLOR_GREEN;
      } else {
        return COLOR_ORANGE;
      }
    }
  }

  Icon getIcon(DateTime now, Task t) {
    IconData id = Icons.circle;
    if (t.startTime.add(Duration(minutes: t.duration)).compareTo(now) >= 0) {
      id = Icons.circle_outlined;
    }
    return Icon(id, color: Colors.black);
  }

  Color getTextColor(DateTime now, Task t, int index) {
    if (t.startTime.add(Duration(minutes: t.duration)).compareTo(now) >= 0) {
      return Colors.white;
    }
    return Colors.grey;
  }
}
